# Flask API with Authentication

run [sql_init.py](sql_init.py) to initialize db.

## `curl` commands

```bash
# register new user
curl -X POST http://localhost:5000/register -H "content-type: application/json" -d '{"name":"<USERNAME>", "password":"<PASSWORD>"}'

# login (existing user)
curl --user admin:ke1nes -X POST http://localhost:5000/login
curl http://localhost:5000/v1/user -H "x-access-tokens: <TOKEN>"
```


## Sources

* [Flask JWT](https://www.bacancytechnology.com/blog/flask-jwt-authentication)
* [Flask CORS](https://flask-cors.readthedocs.io/en/latest/)
* [PyJWT](https://pyjwt.readthedocs.io/en/latest/usage.html)
* [Node JWT](https://medium.com/techiediaries-com/angular-9-8-authentication-form-angular-formbuilder-formgroup-and-validators-example-with-node-f91729db006f)
* [Flask project structure](https://www.digitalocean.com/community/tutorials/how-to-structure-a-large-flask-application-with-flask-blueprints-and-flask-sqlalchemy)