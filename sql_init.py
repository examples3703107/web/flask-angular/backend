from werkzeug.security import generate_password_hash

from app import create_app, db
from app.models.auth import Role, User

with create_app().app_context():
    db.drop_all()
    db.create_all()

    roles = [
        Role(name='admin'),
        Role(name='user'),
    ]
    users = [
        User(name='admin', password=generate_password_hash('ke1nes', method='sha256'), roles=roles),
        User(name='user', password=generate_password_hash('ke1nes', method='sha256'), roles=[roles[1]]),
    ]

    for role in roles:
        db.session.add(role)
        db.session.commit()

    for user in users:
        db.session.add(user)
        db.session.commit()
