from dataclasses import dataclass

from flask import Request


class LoginRequestError(Exception):
    pass


@dataclass
class LoginRequest:
    username: str
    password: str

    @staticmethod
    def create(req: Request):
        auth = None
        if req.authorization:
            auth = AuthorizationRequest(req)
        if req.is_json:
            auth = JsonRequest(req)
        if req.form:
            auth = FormRequest(req)
        if not auth or not auth.username or not auth.password:
            raise LoginRequestError()
        return auth


class AuthorizationRequest(LoginRequest):
    def __init__(self, req: Request):
        self.username = req.authorization.username
        self.password = req.authorization.password


class JsonRequest(LoginRequest):
    def __init__(self, req: Request):
        self.username = req.json.get('username')
        self.password = req.json.get('password')


class FormRequest(LoginRequest):
    def __init__(self, req: Request):
        self.username = req.form.get('username')
        self.password = req.form.get('password')
