from functools import wraps

import jwt
from flask import request, jsonify
from jwt.exceptions import InvalidTokenError

from app.models.auth import User
from config import Config


def token_required(f):
    def bearer_token():
        auth_header = request.headers.get('Authorization')
        if auth_header and auth_header.lower().startswith('bearer'):
            return auth_header.split(" ")[1]

    def x_access_token():
        if 'x-access-tokens' in request.headers:
            return request.headers['x-access-tokens']

    @wraps(f)
    def decorator(*args, **kwargs):
        if not (token := bearer_token() or x_access_token()):
            return jsonify({'message': 'a valid token is required!'}), 403
        try:
            data = jwt.decode(token, Config.SECRET_KEY, algorithms=['HS256'])
            current_user = User.query.filter_by(id=data['user']).first()
        except InvalidTokenError:
            return jsonify({'message': 'a valid token is required!'}), 403

        return f(current_user, *args, **kwargs)

    return decorator


def user_required(f):
    @token_required
    @wraps(f)
    def decorator(current_user: User, *args, **kwargs):
        if not current_user.is_user():
            return jsonify({'message': 'user role required!'}), 403

        return f(current_user, *args, **kwargs)

    return decorator


def admin_required(f):
    @token_required
    @wraps(f)
    def decorator(current_user: User, *args, **kwargs):
        if not current_user.is_admin():
            return jsonify({'message': 'invalid role'}), 403

        return f(current_user, *args, **kwargs)

    return decorator
