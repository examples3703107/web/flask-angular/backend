import jwt
from flask import request, jsonify, make_response
from werkzeug.security import generate_password_hash, check_password_hash

from app.auth import bp
from app.auth.login_request import LoginRequest, LoginRequestError
from app.extension import db
from app.models.auth import Role, User
from config import Config


@bp.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    hashed_pw = generate_password_hash(data['password'], method='sha256')

    admin = Role.query.filter_by(name='admin').first()
    user = Role.query.filter_by(name='user').first()

    roles = [user]
    if 'admin' in data['name']:
        roles.append(admin)

    new_user = User(name=data['name'], password=hashed_pw, roles=roles)
    db.session.add(new_user)
    db.session.commit()
    return jsonify({'message': 'registered successfully'})


@bp.route('/login', methods=['POST'])
def login():
    try:
        auth = LoginRequest.create(request)
    except LoginRequestError:
        return make_response('could not verify', 401, {'Authentication': 'login required'})

    user = User.query.filter_by(name=auth.username).first()
    if check_password_hash(user.password, auth.password):
        token = jwt.encode(user.jwt_payload, Config.SECRET_KEY, 'HS256')

        return jsonify({'token': token})

    return make_response('could not verify', 401, {'Authentication': 'login required'})
