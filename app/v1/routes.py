from app.auth.decorators import admin_required, user_required, token_required
from app.models.auth import User
from app.v1 import bp


@bp.route('/')
def index():
    return 'v1 blueprint'


@bp.route('/public', methods=['GET'])
def public():
    return "this is public"


@bp.route('/user', methods=['GET'])
@token_required
def user(current_user: User):
    return f"hello {current_user.name}"


@bp.route('/me', methods=['GET'])
@user_required
def me(current_user: User):
    return f"hello {current_user.name} [{current_user.id}]"


@bp.route('/admin', methods=['GET'])
@admin_required
def admin(current_user: User):
    return f"hello admin {current_user.name} [{current_user.id}]"
