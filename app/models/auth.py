import datetime

from app.extension import db

user_roles = db.Table('user_roles',
                      db.Column('user_id', db.Integer, db.ForeignKey('users.id'), primary_key=True),
                      db.Column('role_id', db.Integer, db.ForeignKey('roles.id'), primary_key=True))


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False, unique=True)

    def __repr__(self):
        return f'<Role "{self.name}">'


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(50), nullable=False)
    roles = db.relationship(Role, secondary=user_roles, lazy='subquery', backref=db.backref('users', lazy=True))

    @property
    def jwt_payload(self):
        return {'user': self.id, 'roles': [role.name for role in self.roles],
                'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}

    def is_user(self):
        return 'user' in [role.name for role in self.roles]

    def is_admin(self):
        return 'admin' in [role.name for role in self.roles]

    def __repr__(self):
        return f'<User "{self.name}">'
