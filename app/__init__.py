from flask import Flask
from flask_cors import CORS

from app.extension import db
from config import Config


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    # Initialize extensions
    CORS(app, resources={r"/*": {"origins": "*"}})
    db.init_app(app)

    # Register blueprints
    from app.auth import bp as auth_bp
    app.register_blueprint(auth_bp)

    from app.v1 import bp as main_bp
    app.register_blueprint(main_bp)

    @app.route('/')
    def hello_world():
        return 'Hello from Flask!'

    return app
